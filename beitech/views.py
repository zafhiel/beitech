import coreapi
from django.shortcuts import render
from rest_framework import generics
from .models import Customer, Order
from rest_framework.schemas import AutoSchema
from .serializers import CustomerSerializer, OrderSerializer


class RESTCustomerList(generics.ListAPIView):
    """
        Formato compatible para que la libreria Jquery:select2 pueda filtrar los customers.
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    schema = AutoSchema(
        manual_fields=[
            coreapi.Field("q", required=False, location='query',
                          description='If it is numeric filter by customer ID, if it is text by name.'),
        ]
    )

    def get_queryset(self):
        query = self.request.query_params.get('q')

        if query in ('', None):
            queryset = Customer.objects.all().order_by('name')
        else:
            if query.isdigit():
                queryset = Customer.objects.filter(customer_id=query).order_by('name')
            else:
                queryset = Customer.objects.filter(name__contains=query).order_by('name')

        return queryset


class RESTOrderList(generics.ListCreateAPIView):
    """
        Formato compatible para que las ordenes puedan ser filtradas por clientes y rangos de fecha.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    schema = AutoSchema(
        manual_fields=[
            coreapi.Field("customer_id", required=False, location='query',
                          description='Filter by customer ID', type='integer'),
            coreapi.Field("date_from", required=False,
                          location='query',
                          description='Filter if the date is greater than or equal', example='2019-08-01'),
            coreapi.Field("date_to", required=False,
                          location='query',
                          description='Filter if the date is less than or equal.', example='2019-08-06')
        ]
    )

    def get_queryset(self):
        customer = self.request.query_params.get('customer')
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')

        filter_params = {}

        if customer not in ('', None):
            filter_params['customer_id'] = int(customer)

        if date_from not in ('', None) and date_to in ('', None):
            filter_params['date__gte'] = date_from

        if date_from in ('', None) and date_to not in ('', None):
            filter_params['date__lte'] = date_to

        if date_from not in ('', None) and date_to not in ('', None):
            filter_params['date__range'] = [date_from, date_to]

        if len(filter_params) == 0:
            queryset = Order.objects.all().order_by('order_id')
        else:
            queryset = Order.objects.filter(**filter_params).order_by('order_id')

        return queryset


def index(request):
    return render(request, 'beitech/index.html')
