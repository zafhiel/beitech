from django.db import models


class Customer(models.Model):
    """
        Tabla Customer Originalmente:

        CREATE TABLE "customer"(
            customer_id INTEGER NOT NULL CONSTRAINT CUSTOMER_PK PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL
        )
    """
    customer_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)

    @property
    def id(self):
        """
            Metodo para que los detalles de la orden seran mostrador en formato JSON
        """
        return self.customer_id

    @property
    def text(self):
        """
            Metodo para que los detalles de la orden seran mostrador en formato JSON
        """
        return f'({self.customer_id}) - {self.name}'

    class Meta:
        db_table = 'beitech_customer'
        ordering = ['name']

    def __str__(self):
        return f'({self.customer_id}) - {self.name}'


class Product(models.Model):
    """
    Tabla Product Originalmente:

        CREATE TABLE product(
            product_id INTEGER NOT NULL CONSTRAINT PRODUCT_PK PRIMARY KEY,
            name VARCHAR(255) NOT NULL
        );
    """
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.IntegerField()
    product_description = models.CharField(max_length=255)

    class Meta:
        db_table = 'beitech_product'

    def __str__(self):
        return f'({self.product_id}) - {self.name}'


class ProductCustomer(models.Model):
    """
        Tabla donde se almacenan los productos que puede comprar cada cliente.
    """
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)

    class Meta:
        db_table = 'beitech_product_customer'
        unique_together = (("customer", "product"),)

    def __str__(self):
        return f'[{self.id}] ({self.customer.customer_id}) {self.customer.name} - ({self.product.product_id}) - {self.product.name}'


class Order(models.Model):
    """
        Tabla Order Originalmente:

        CREATE TABLE "order"(
            order_id INTEGER NOT NULL CONSTRAINT ORDER_PK PRIMARY KEY,
            customer_id INTEGER CONSTRAINT ORDER_CUSTOMER_CUSTOMER_ID_FK REFERENCES CUSTOMER,
            delivery_address INTEGER NOT NULL
        )
    """
    order_id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    delivery_address = models.CharField(max_length=255)
    date = models.DateField()

    @property
    def products(self):
        """
            Metodo para que los detalles de la orden seran mostrador en formato JSON
        """
        return [{"id": od.product.product_id,
                 "name": od.product.name,
                 "price": od.product.price,
                 "quantity": od.quantity} for od in self.orderdetail_set.all()]

    class Meta:
        db_table = 'beitech_order'

    def __str__(self):
        return f'[{self.order_id}] ({self.customer.customer_id}) {self.customer.name}'


class OrderDetail(models.Model):
    """
        Tabla OrderDetail Originalmente:

        CREATE TABLE order_detail(
            product_description VARCHAR(255) NOT NULL,
            price iNTEGER NOT NULL,
            order_id INTEGER NOT NULL CONSTRAINT ORDER_DETAIL_ORDER_ORDER_ID_FK REFERENCES "ORDER"
        )
    """
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField()

    class Meta:
        db_table = 'beitech_order_detail'

    def __str__(self):
        return f'[{self.order.order_id}] ({self.product.product_id}) - {self.product.name}'
