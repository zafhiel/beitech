# Generated by Django 2.2.4 on 2019-08-04 01:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('customer_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'beitech_customer',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('order_id', models.AutoField(primary_key=True, serialize=False)),
                ('delivery_address', models.CharField(max_length=255)),
                ('date', models.DateField()),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='beitech.Customer')),
            ],
            options={
                'db_table': 'beitech_order',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('product_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('price', models.IntegerField()),
                ('product_description', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'beitech_product',
            },
        ),
        migrations.CreateModel(
            name='OrderDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='beitech.Order')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='beitech.Product')),
            ],
            options={
                'db_table': 'beitech_order_detail',
            },
        ),
        migrations.CreateModel(
            name='ProductCustomer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='beitech.Customer')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='beitech.Product')),
            ],
            options={
                'db_table': 'beitech_product_customer',
                'unique_together': {('customer', 'product')},
            },
        ),
    ]
