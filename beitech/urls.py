from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path
from .views import *

schema_view = get_swagger_view(title='Beitech API')

urlpatterns = [path('', index),
               path('admin/', admin.site.urls),
               path("api/customer/", RESTCustomerList.as_view(), name="rest_bulletin_list"),
               path("api/order/", RESTOrderList.as_view(), name="rest_bulletin_list"),
               path('docs/', schema_view),
               ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
