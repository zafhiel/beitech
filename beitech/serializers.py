from rest_framework import serializers
from .models import Customer, Order, OrderDetail, Product, ProductCustomer


class CustomerSerializer(serializers.ModelSerializer):
    """
        Informacion necesaria para ser consumida por el frontend:select2
    """
    class Meta:
        model = Customer
        fields = ["id", "text"]


class OrderSerializer(serializers.ModelSerializer):
    """
        Se sobreescribe el metodo 'create' para interceptar el json
        capturado y guardar en 2 modelos diferentes (Order, OrderDetails)
    """
    products = serializers.JSONField(help_text='JSON or JSONList Example: <br><br>* {"id": 2,"quantity": 3}'
                                               '<br>* [{"id": 1,"quantity": 5}, {"id": 6,"quantity": 11}]')

    def create(self, validated_data):
        """
            Validaciones:

                * Productos unicos en una lista de JSON y y campos (Ids y Cantidades) validos
                * Que tenga al menos un producto por orden
                * Que no tenga mas de 5 productos por orden
                * Que el cliente tenga relacionado los productos
        """

        # Validacion: Productos unicos en una lista de JSON y campos (Ids y Cantidades) validos
        unique_product = validated_data.get('products')
        products = {}

        if isinstance(unique_product, list):
            for product in unique_product:
                if not self.JSONSchemaValidator(product):
                    raise serializers.ValidationError("The delivered products do not comply with the required scheme.")

                if product['id'] in products:
                    products[product['id']] += product['quantity']
                else:

                    products[product['id']] = product['quantity']
        else:
            if not self.JSONSchemaValidator(unique_product):
                raise serializers.ValidationError("The delivered products do not comply with the required scheme.")

            products[unique_product['id']] = unique_product['quantity']

        # Validacion: Que no tenga al menos 1 producto por orden
        if len(products) == 0:
            raise serializers.ValidationError("To place an order you must have at least 1 product.")

        # Validacion: Que no tenga mas de 5 productos por orden
        customer = validated_data.get('customer')

        if len(products) > 5:
            raise serializers.ValidationError("No more than 5 products per order.")

        # Validacion: Que el cliente tenga relacionado los productos
        if ProductCustomer.objects.filter(customer=customer, product_id__in=products.keys()).count() != len(products):
            raise serializers.ValidationError("You are using a restricted product for this user.")

        new_order = Order()
        new_order.customer = customer
        new_order.delivery_address = validated_data.get('delivery_address')
        new_order.date = validated_data.get('date')
        new_order.save()

        for product, quantity in products.items():
            new_order_detail = OrderDetail()
            new_order_detail.product_id = product
            new_order_detail.quantity = quantity
            new_order_detail.order = new_order
            new_order_detail.save()

        return new_order

    @staticmethod
    def JSONSchemaValidator(json):
        """
            Validaciones Adicionales:

                * Si contiene las llaves establecidas
                * Que el producto exista
                * Que el tipo de dato de quantity sera int
                * Que quantity tenga un valor mayor a cero
        """
        if not all(k in json for k in ("id", "quantity")):
            return False

        if not Product.objects.filter(product_id=json['id']).exists():
            return False

        if not isinstance(json['quantity'], int):
            return False

        if json['quantity'] <= 0:
            return False

        return True

    class Meta:
        model = Order
        fields = ('order_id', 'customer', 'delivery_address', 'date', 'products')
