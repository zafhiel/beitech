Beitech
=======


### Resumen Solución:

Se modificó la base de datos de la siguiente manera:


1. Se creó la tabla PRODUCT_CUSTOMER con las columnas CUSTOMER_ID
  y PRODUCT_ID.
2. Se movieron las columnas PRICE y PRODUCT_DESCRIPTION de la tabla
  ORDER_DETAIL a la tabla PRODUCTS.
3. Se agrego la columna DATE en la tabla ORDER.
4. Se agregó la columnas PRODUCT_ID y QUANTITY a la tabla ORDER_DETAIL.


Se implementó un servicio web con la librería ***djangorestframework*** el cual
por métodos http puede crear y consultar una ORDEN la cual tiene validaciones requeridas para
cumplir con el objetivo del ejercicio, también puede consultar los CUSTOMERS.

Se implementa un frontend con base ***bootstrap*** con las características requeridas en el ejercicio.




### Diagrama Actualizado:

![alt text](diagrama.png "Diagrama")


[Archivo SQL](data.sql)  con sentencias de creación de tablas y sus datos de prueba.



### Documentacion API:

![alt text](api_documentation.png "Api Documentation")

Implementada la utilidad de ***Swagger*** para documentar API.
### Instrucciones para ejecución:

* Descargar repositorio.

```bash
git clone https://gitlab.com/zafhiel/beitech.git
```

* Abrir carpeta descargada.

```bash
cd beitech
```
* Crear ambiente virtual de python.

```bash
virtualenv env
```

* Activar ambiente virtual.

```bash
# linux y mac
source env/bin/activate

# windows
env/bin/activate.bat
```

* Instalar librerias necesarias.

```bash
pip install -r requirements.txt

# Django==2.2.4
# djangorestframework==3.10.2
# django-rest-swagger==2.2.0
```


* Crear tablas en base de datos de sqlite3.

```bash
python manage.py migrate
```


* Importar datos de customers, products y productscustomers.

```bash
python manage.py loaddata data.json

# mocks generados desde mockaroo.com
# 100 registros para: Model.Customer
# 100 registros para: Model.Product
# 500 registros para: Model.ProductCustomer
```

* Recolectar archivos estáticos.

```bash
python manage.py collectstatic
```

* ***Opcional:*** Si vas a usar el administrador no olvides crear el ***superuser***.

```bash
python manage.py createsuperuser
```


* Correr servidor.

```bash
python manage.py runserver 0.0.0.0:8000
```



### Datos para pruebas:

A continuación puede ejecutar la siguiente consulta para saber
cuales son los usuarios que más tienen productos y sus respectivos IDs.

```sqlite
SELECT customer_id,
      GROUP_CONCAT(product_id) AS products_ids,
      count(*) AS totalproducts
FROM beitech_product_customer
GROUP BY customer_id
ORDER BY totalproducts DESC
```

Aquí muestro los primeros 10 resultados  de los datos mocks.


| CustomerID | Products IDs | Total Products |
| ---------- | -------------| -------------- |
|56    |6,65,53,71,2,27,75,44,37,14   |10|
|68    |43,63,13,96,58,9,85,62,89,57  |10|
|81    |30,57,48,1,51,22,20,75,46,33  |10|
|8 |1,70,95,73,35,38,53,6,48  |9|
|36    |39,52,61,22,38,15,85,58,97    |9|
|72    |99,46,95,72,57,41,17,78,8 |9|
|99    |77,69,21,41,29,8,19,64,79 |9|
|10    |97,7,15,28,39,21,58,100   |8|
|30    |37,4,45,84,35,78,16,82    |8|
|63    |11,42,19,49,96,20,31,26   |8|

Have fun! :thumbsup: